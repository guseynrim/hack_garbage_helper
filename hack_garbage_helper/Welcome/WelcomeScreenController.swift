//
//  WelcomeScreenController.swift
//  hack_garbage_helper
//
//  Created by Сеня Римиханов on 01.12.2019.
//  Copyright © 2019 Гусейн Римиханов. All rights reserved.
//

import Foundation
import UIKit

class WelcomeScreenController: UIViewController {
    
    private let welcomeLabel: UILabel = {
        let label = UILabel()
        label.text = "Привет, Гурам"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        label.textColor = UIColor.white
        return label
    }()
    
    private let mainImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "145 1")
        return imageView
    }()
    
    private let titleLabel: UILabel = {
          let label = UILabel()
           label.text = "Приступай к своим задачам на день"
           label.translatesAutoresizingMaskIntoConstraints = false
           label.font = UIFont.systemFont(ofSize: 30, weight: .bold)
           label.textColor = UIColor.white
        label.numberOfLines = 0
           return label
       }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Мы поможем тебе выполнить их быстро и качественно"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.textColor = UIColor.gray
        label.numberOfLines = 0
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
            let destination = UINavigationController(rootViewController: TasksController())
            destination.modalPresentationStyle = .fullScreen
            self.present(destination, animated: true, completion: nil)
        })
    }
    
}

extension WelcomeScreenController: ControllerSetup {
    func setup() {
        self.view.backgroundColor = UIColor.black
        self.view.addSubview(welcomeLabel)
        self.view.addSubview(mainImageView)
        self.view.addSubview(titleLabel)
        self.view.addSubview(subtitleLabel)
        welcomeLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 32).isActive = true
        welcomeLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        welcomeLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -32).isActive = true
        
        
        
        subtitleLabel.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -40).isActive = true
        subtitleLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 32).isActive = true
        subtitleLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -32).isActive = true
        
        titleLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 32).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -32).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: subtitleLabel.topAnchor, constant: -10).isActive = true
        
        mainImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        mainImageView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.8).isActive = true
        mainImageView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.25).isActive = true
        mainImageView.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -150).isActive = true
        
        
        
        
    }
    
    
}
