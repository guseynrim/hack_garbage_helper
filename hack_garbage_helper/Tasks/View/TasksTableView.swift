//
//  TasksTableView.swift
//  hack_garbage_helper
//
//  Created by Сеня Римиханов on 30.11.2019.
//  Copyright © 2019 Гусейн Римиханов. All rights reserved.
//

import Foundation
import UIKit


class TasksTableView: UITableView {
    
    var tasks = [Task]()
    weak var openVCDelegate: OpenControllerDelegate?
    
    init() {
        super.init(frame: .zero, style: .grouped)
        translatesAutoresizingMaskIntoConstraints = false
        separatorColor = UIColor.clear
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        delegate = self
        dataSource = self
        sectionFooterHeight = 0
        // MARK: – CELL REGISTER
        register(UINib(nibName: "TaskTableCell", bundle: nil), forCellReuseIdentifier: "TaskCell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension TasksTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openVCDelegate?.openVC()
    }
    
}

extension TasksTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TaskTableCell
        cell.viewModel = (title: "HELLO", subtitle: "testtest")
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
}
