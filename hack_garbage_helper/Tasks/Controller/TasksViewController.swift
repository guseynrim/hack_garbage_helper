//
//  TasksViewController.swift
//  hack_garbage_helper
//
//  Created by Сеня Римиханов on 30.11.2019.
//  Copyright © 2019 Гусейн Римиханов. All rights reserved.
//

import Foundation
import UIKit

protocol ControllerSetup {
    func setup()
}

protocol OpenControllerDelegate: class {
    func openVC()
}


class TasksController: UIViewController {
    
    let tasksTableView = TasksTableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tasksTableView.openVCDelegate = self
        setup()
        
    }
    
}

extension TasksController: ControllerSetup {
    func setup() {
        self.view.addSubview(tasksTableView)
        NSLayoutConstraint.activate([
            tasksTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            tasksTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tasksTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tasksTableView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }
    
    
}

extension TasksController: OpenControllerDelegate {
    func openVC() {
        let mapController = MapController()
        mapController.modalPresentationStyle = .fullScreen
        //self.present(mapController, animated: true, completion: nil)
        self.navigationController?.pushViewController(mapController, animated: true)
    }
    
    
}


