//
//  QRAlert.swift
//  hack_garbage_helper
//
//  Created by Сеня Римиханов on 01.12.2019.
//  Copyright © 2019 Гусейн Римиханов. All rights reserved.
//

import Foundation
import UIKit


class QRAlert: UIView {
    
    private let leftImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "Oval")
        return imageView
    }()
    
    private let messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.numberOfLines = 2
        label.textColor = UIColor.black
        label.text = "Просканируй QR-код на транспортном средстве"
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.white
        layer.cornerRadius = 8
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension QRAlert {
    private func setup() {
        addSubview(leftImageView)
        addSubview(messageLabel)
        
        leftImageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        leftImageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        leftImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        leftImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        messageLabel.leadingAnchor.constraint(equalTo: leftImageView.trailingAnchor, constant: 15).isActive = true
        messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        messageLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        
    }
}
