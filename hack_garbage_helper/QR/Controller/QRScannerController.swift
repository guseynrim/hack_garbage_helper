//
//  QRScannerController.swift
//  hack_garbage_helper
//
//  Created by Сеня Римиханов on 01.12.2019.
//  Copyright © 2019 Гусейн Римиханов. All rights reserved.
//

import Foundation
import UIKit


class QRScannerController: UIViewController {
    
    private var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    
    private let alertMessage = QRAlert()
    
    private var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                print(qrData)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scannerView = QRScannerView()
        
        self.view.addSubview(scannerView)
        self.view.addSubview(alertMessage)
        
        scannerView.translatesAutoresizingMaskIntoConstraints = false
        scannerView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        scannerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        scannerView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        scannerView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
        alertMessage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        alertMessage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        alertMessage.heightAnchor.constraint(equalToConstant: 52).isActive = true
        alertMessage.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 70).isActive = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    
    
}

extension QRScannerController: QRScannerViewDelegate {
    func qrScanningDidStop() {
     
       }
       
       func qrScanningDidFail() {
           //presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
       }
       
       func qrScanningSucceededWithCode(_ str: String?) {
        self.qrData = QRData(codeString: str)
       }
}
