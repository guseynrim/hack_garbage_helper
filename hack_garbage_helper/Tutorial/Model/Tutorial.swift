//
//  Tutorial.swift
//  hack_garbage_helper
//
//  Created by Сеня Римиханов on 30.11.2019.
//  Copyright © 2019 Гусейн Римиханов. All rights reserved.
//

import Foundation


struct Tutorial {
    let id: Int
    let htmlContents: String
}
