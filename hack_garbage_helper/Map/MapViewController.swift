//
//  MapViewController.swift
//  hack_garbage_helper
//
//  Created by Сеня Римиханов on 30.11.2019.
//  Copyright © 2019 Гусейн Римиханов. All rights reserved.
//

import Foundation
import UIKit
import YandexMapKit
import YandexMapKitDirections



class MapController: UIViewController {
    
    private var mapView: YMKMapView!
    
    //55.859842, 37.601566
    //55.859336, 37.610578
    let ROUTE_START_POINT = YMKPoint(latitude: 55.859842, longitude: 37.601566)
    let ROUTE_END_POINT = YMKPoint(latitude: 55.859336, longitude: 37.610578)
    let TARGET_LOCATION = YMKPoint(latitude: 55.858358, longitude: 37.606201)
     var drivingSession: YMKDrivingSession?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        let requestPoints : [YMKRequestPoint] = [
            YMKRequestPoint(point: ROUTE_START_POINT, type: .waypoint, pointContext: nil),
                   YMKRequestPoint(point: ROUTE_END_POINT, type: .waypoint, pointContext: nil),
                   ]
        
        let responseHandler = {(routesResponse: [YMKDrivingRoute]?, error: Error?) -> Void in
            if let routes = routesResponse {
                self.onRoutesReceived(routes)
            } else {
                self.onRoutesError(error!)
            }
        }
        
        let drivingRouter = YMKDirections.sharedInstance().createDrivingRouter()
        drivingSession = drivingRouter.requestRoutes(
            with: requestPoints,
            drivingOptions: YMKDrivingDrivingOptions(),
            routeHandler: responseHandler)
        
        
        
        mapView.mapWindow.map.move(
                with: YMKCameraPosition(target: TARGET_LOCATION, zoom: 15, azimuth: 0, tilt: 0),
                animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 2),
                cameraCallback: nil)
        
        
        }
    
    
    
    
    func onRoutesReceived(_ routes: [YMKDrivingRoute]) {
          let mapObjects = mapView.mapWindow.map.mapObjects
          for route in routes {
            let polygon = route.geometry
            
            let polygonMapObject =  self.mapView.mapWindow.map.mapObjects.addPolyline(with: polygon)
            
            polygonMapObject.strokeColor = UIColor.red
            
           
      }
    }
      
      func onRoutesError(_ error: Error) {
          let routingError = (error as NSError).userInfo[YRTUnderlyingErrorKey] as! YRTError
          var errorMessage = "Unknown error"
          if routingError.isKind(of: YRTNetworkError.self) {
              errorMessage = "Network error"
          } else if routingError.isKind(of: YRTRemoteError.self) {
              errorMessage = "Remote server error"
          }
          
          let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
          
          present(alert, animated: true, completion: nil)
      }
    
    
}

extension MapController: ControllerSetup {
    
    
    func setup() {
        self.mapView = YMKMapView(frame: UIScreen.main.bounds)
        mapView.mapWindow.map.isNightModeEnabled = false
        
//        let points = [
//            YMKPoint(latitude: 55.537956, longitude: 37.221679),
//            YMKPoint(latitude: 55.537956, longitude: 38.270874),
//            YMKPoint(latitude: 56.004524, longitude: 38.270874),
//            YMKPoint(latitude: 56.004524, longitude: 37.221679),
//            YMKPoint(latitude: 55.537956, longitude: 37.221679),
//        ]
//        let polygon = YMKPolygon(outerRing: YMKLinearRing(points: points), innerRings: [])
//        let polygonMapObject = self.mapView.mapWindow.map.mapObjects.addPolygon(with: polygon)
//        polygonMapObject.fillColor = UIColor.green.withAlphaComponent(0.16)
//        polygonMapObject.strokeWidth = 3.0
//        polygonMapObject.strokeColor = .green
        self.view.addSubview(mapView)
        
//        NSLayoutConstraint.activate([
//                 mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
//                 mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
//                 mapView.leftAnchor.constraint(equalTo: view.leftAnchor),
//                 mapView.rightAnchor.constraint(equalTo: view.rightAnchor)
//             ])
    }
    
    
    
    
}
